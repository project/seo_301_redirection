<?php

/**
 * @file
 * The SEO 301 redirect module.
 *
 * It allows site administrators to apply 301 redirection for following options.
 * 1. Remove Trailing Slash.
 * 2. Convert URL to lower case.
 * 3. Remove index.php from URL.
 */

/**
 * Implements hook_help().
 */
function seoredirect_help($path, $arg) {
  switch ($path) {
    case "admin/help#iaseo":
      return '<p>' . t('This module will help you to resolve below SEO recommendations:') . '</p><ol><li>' . t('Remove Trailing Slash') . '</li><li>' . t('Convert URL to lower case') . '</li><li>' . t('Remove index.php from URL.') . '</li></ol>';
  }
}

/**
 * Implements hook_menu().
 */
function seoredirect_menu() {
  $items = array();
  $items['admin/config/search/seoredirect'] = array(
    'title' => 'SEO 301 Redirection',
    'description' => 'Use this form setting to apply SEO Recommendations.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'seoredirect_form',
    ),
    'access callback' => 'user_access',
    'access arguments' => array(
      'administer users',
    ),
    'file' => 'seoredirect.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_init().
 */
function seoredirect_init() {
  global $base_url;
  $protocol   = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === TRUE ? 'https://' : 'http://';
  $currentUrl = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $exceptionStrings = array(
    "/admin/",
    "/ajax/",
    "sites/default/files/",
  );
  $seoredirect_url_substr = variable_get('seoredirect_url_substr');
  if (trim($seoredirect_url_substr) != "") {
    $stringUrls = explode("\r\n", $seoredirect_url_substr);
    foreach ($stringUrls as $value) {
      $chkVal = trim(drupal_html_to_text($value));
      if (!in_array($chkVal, $exceptionStrings)) {
        $exceptionStrings[] = $chkVal;
      }
    }
  }
  // URL flag to convert lower case.
  $flag = 1;
  foreach ($exceptionStrings as $value) {
    if (stristr($currentUrl, $value)) {
      // URL will not converted to lower case.
      $flag = 0;
      break;
    }
  }
  if ($flag) {
    $settings = variable_get('seoredirect_options');
    if (substr($currentUrl, -1) == '/' && ($currentUrl != $base_url . '/') && isset($settings["trailingslash"])) {
      // Remove trailing slash.
      if ($settings["trailingslash"]) {
        if ($settings["urllower"]) {
          seoredirect_redirect_page(rtrim(strtolower($currentUrl), '/'));
        }
        else {
          seoredirect_redirect_page(rtrim($currentUrl, '/'));
        }
      }
    }
    elseif ($currentUrl == $base_url . '/index.php' && isset($settings["removeindex"])) {
      // Remove index.php.
      if ($settings["removeindex"]) {
        seoredirect_redirect_page($base_url);
      }
    }
    elseif ($currentUrl != strtolower($currentUrl)) {
      // Convert URLs to lower case URL.
      if ($settings["urllower"]) {
        seoredirect_redirect_page(strtolower($currentUrl));
      }
    }
  }
}

/**
 * Function seoredirect_redirect_page() is used to redirect URL.
 */
function seoredirect_redirect_page($path, $statuscode = 301) {
  $options = array();
  drupal_goto($path, $options, $statuscode);
  exit;
}
