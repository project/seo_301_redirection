<?php

/**
 * @file
 * Configuration settings for administrator.
 */

/**
 * Implements hook_form().
 */
function seoredirect_form($form, &$form_state) {
  $form['#attributes'] = array(
    'enctype' => 'multipart/form-data',
  );
  // The options to display checkboxes.
  $options = array(
    'trailingslash' => t('Remove Trailing Slash From URL.'),
    'removeindex' => t('Remove index.php from URL.'),
    'urllower' => t('Convert all URLs to lower case.'),
  );
  // The drupal checkboxes form field definition.
  $form['seoredirect_options'] = array(
    '#title' => '',
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => variable_get('seoredirect_options', array(
      'page',
    )),
    '#prefix' => "<strong>This form will do 301 redirection for following options.</strong>",
  );
  // Textarea input field.
  $form['seoredirect_url_substr'] = array(
    '#type' => 'textarea',
    '#title' => t('Enter substring of URLs which will not be converted to lower case in new lines.'),
    '#default_value' => variable_get('seoredirect_url_substr', array(
      'page',
    )),
    '#suffix' => "<em style='color:green;'>URLs which contain strings <strong>/admin/</strong> or <strong>/ajax/</strong> or <strong>sites/default/files</strong> are not converted to lower case.</em>",
    '#attributes' => array(
      "placeholder" => "/admin/\n/ajax/\nsites/default/files",
    ),
  );
  return system_settings_form($form, TRUE);
}
