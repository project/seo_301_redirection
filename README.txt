
SEO 301 Redirection README

CONTENTS OF THIS FILE
----------------------

  * Introduction
  * Installation


INTRODUCTION
------------
It allows site administrators to apply 301 redirection for following options.
1. Remove Trailing Slash.
2. Convert URL to lower case.
3. Remove index.php from URL.

Maintainer: Sharad Tikadia (https://www.drupal.org/u/stikadia)


INSTALLATION
------------
1. Copy seoproof folder to modules (usually sites/all/modules) directory.
2. Go to the admin modules page (admin/modules).
3. Enable 'SEO 301 Redirection'.
